using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Animator anim;

    private float hp;
    private bool isDead => hp <=0;

    private string currentAnim;

    private void Start()
    {
        OnInit();
    }
    public virtual void OnInit()
    {
        hp = 100;
    }
    public virtual void OnDespam()
    {

    }
    protected virtual void OnDeath()
    {

    }
    protected void ChangeAnim(string aniName)
    {
        if (currentAnim != aniName)
        {
            anim.ResetTrigger(aniName);
            currentAnim = aniName;
            anim.SetTrigger(currentAnim);

        }

    }
    public void OnHit(float damge)
    {
        if(hp >= damge)
        {
            hp -= damge;
            if (isDead)
            {
                OnDeath();
            }
        }
    }
   
}
