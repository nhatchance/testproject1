using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField] private Rigidbody2D rb;
    public LayerMask groundLayer;
    [SerializeField] private float speed =200f;
    [SerializeField] public float jumpForce = 400f;
    private float horizontal;
    private bool isGrounded = true;
    private bool isJumping = false;
    private bool isAttack = false;
    private bool isDie = false;
    private int coin;
    private string currentAnim;
    private Vector3 savePoint;

    // Start is called before the first frame update
    void Start()
    {
        SavePoint();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontal = Input.GetAxisRaw("Horizontal");

        isGrounded = CheckGround();

        if (isAttack)
        {
            rb.velocity = Vector2.zero;
            return;
        }
        if (isDie)
        {
            return;
        }
        if (isGrounded)
        {
            if (isJumping)
            {
                return;
            }
            // Jump
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {
                Jump();
            }

            // Change anim Attack
            if(Input.GetKeyDown(KeyCode.F) && isGrounded)
            {
                Attack();
            }
            if (Input.GetKeyDown(KeyCode.G) && isGrounded)
            {
                Throw();
            }
            // Change anim Run
            if (Mathf.Abs(horizontal) > 0.1f)
            {
                ChangeAnim("run");
            }

            //CheckGround Fall
            if (!isGrounded && rb.velocity.y < 0)
            {
                ChangeAnim("fall");
                isJumping = false;
            }
        }



        // Moving
        if(Mathf.Abs(horizontal) > 0.1f)
        {
            rb.velocity = new Vector2(horizontal * speed * Time.fixedDeltaTime, rb.velocity.y);

            transform.rotation = Quaternion.Euler(new Vector3(0,horizontal > 0 ? 0 : 180,0));

        }
        else if(isGrounded)
        {
            ChangeAnim("idle");
        }
    }


    public override void OnInit()
    {
        base.OnInit();
        isDie = false;
        isAttack = false;
        transform.position = savePoint;
        ChangeAnim("idle");
    }
    public override void OnDespam()
    {
        base.OnDespam();
    }
    protected override void OnDeath()
    {
        base.OnDeath();
    }
    private bool CheckGround()
    {
        Debug.DrawLine(transform.position, transform.position + Vector3.down * 0.9f, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position ,Vector2.down, 0.9f, groundLayer);

        return hit.collider != null;
    }
    private void Jump()
    {
        isJumping = false;
        ChangeAnim("jump");
        rb.AddForce(Vector2.up * jumpForce);
    }
    private void Attack()
    {
        ChangeAnim("attack");
        isAttack = true;
        Invoke(nameof(ResetAttack), 1f);
    }
    private void Throw()
    {
        ChangeAnim("throw");
        isAttack = true;
        Invoke(nameof(ResetAttack), 1f);

    }
    private void ResetAttack()
    {
        isAttack = false;

        ChangeAnim("idle");
    }
    internal void SavePoint()
    {
        savePoint = transform.position;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            coin++;
            Destroy(collision.gameObject);
        }
        if(collision.CompareTag("DeathZone"))
        {
            isDie = true;
            ChangeAnim("die");
            Invoke(nameof(OnInit), 1f);
        }
            
    }
}
